from fastapi import FastAPI
import pickle
from fastapi.responses import HTMLResponse
import uvicorn

with open("./Recommandations_Hubspot.pickle", "rb") as file:
    Recommandations = pickle.load(file)

app = FastAPI()

@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get('/Product/', response_class=HTMLResponse)
def get_products(ref : str='800053'):
    if(ref in Recommandations.keys()):
        Result = f"""
                    <html>
                        <body>
                            <h1>la référence passée est {ref} et on vous recommande les produits suivants :</h1>
                            <table>
                    """
        for key,value in Recommandations[ref].items():
            Result +="<tr>"
            Result += f"<td> {key} </td>"
            Result += f"<td> {value} </td>"
            Result +="</tr>"
        Result += """
                    </table></body></html>
                    """
        # Result = f"la référence passée est {ref} et on vous recommande les produits suivants : \n {Recommandations[ref]}" 
        
    else:
        Result = f"""
                    <html>
                        <body>
                            <h1>la référence passée est {ref} et on vous recommande les produits suivants :</h1>
                            <table>
                            </table></body></html>
                    """
    return Result

import uvicorn

if __name__ == "__main__":
  uvicorn.run("server.api:app", host="0.0.0.0", port=8000, reload=True)
